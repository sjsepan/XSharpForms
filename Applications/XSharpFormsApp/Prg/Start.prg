USING System
USING System.Windows.Forms
USING System.Drawing

[STAThreadAttribute];
FUNCTION Start( asCmdLine AS STRING[] ) AS INT
	
	LOCAL nExitCode AS INT
	LOCAL oForm AS BasicForm
	
	TRY
	
		nExitCode := 0
		
		Application.EnableVisualStyles()
		Application.DoEvents()
		
		oForm := BasicForm{}
		Application.Run(oForm)
	CATCH ex AS System.Exception
		System.Console.Error.WriteLine(ex:Message)
		nExitCode := 1
	END

RETURN nExitCode

