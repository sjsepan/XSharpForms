USING System
USING System.Threading.Tasks
USING System.Windows.Forms
USING System.Drawing
USING System.Drawing.Printing

CLASS BasicForm INHERIT System.Windows.Forms.Form

PROTECT oToolTip1 AS System.Windows.Forms.ToolTip
PROTECT olblSomeDate AS System.Windows.Forms.Label
PROTECT odtmSomeDate AS System.Windows.Forms.DateTimePicker
PROTECT ochkSomeBool AS System.Windows.Forms.CheckBox
PROTECT otxtSomeString AS System.Windows.Forms.TextBox
PROTECT otxtSomeInt AS System.Windows.Forms.TextBox
PROTECT olblSomeBool AS System.Windows.Forms.Label
PROTECT olblSomeString AS System.Windows.Forms.Label
PROTECT olblSomeInt AS System.Windows.Forms.Label
PROTECT oBtnError AS System.Windows.Forms.Button
PROTECT obtnRun AS System.Windows.Forms.Button
PROTECT obtnColor AS System.Windows.Forms.Button
PROTECT obtnFont AS System.Windows.Forms.Button
PROTECT opicDirty AS System.Windows.Forms.PictureBox
PROTECT opicAction AS System.Windows.Forms.PictureBox
PROTECT olblError AS System.Windows.Forms.Label
PROTECT olblStatus AS System.Windows.Forms.Label
PROTECT oprgProgress AS System.Windows.Forms.ProgressBar
PROTECT oStatusBar1 AS System.Windows.Forms.StatusBar
PROTECT oToolStrip1 AS System.Windows.Forms.ToolStrip
PROTECT oMenuStrip1 AS System.Windows.Forms.MenuStrip
PROTECT otbFileNew AS System.Windows.Forms.ToolStripButton
PROTECT otbFileOpen AS System.Windows.Forms.ToolStripButton
PROTECT otbFileSave AS System.Windows.Forms.ToolStripButton
PROTECT otbFilePrint AS System.Windows.Forms.ToolStripButton
PROTECT otoolbarSeparator1 AS System.Windows.Forms.ToolStripSeparator
PROTECT otbEditUndo AS System.Windows.Forms.ToolStripButton
PROTECT otbEditRedo AS System.Windows.Forms.ToolStripButton
PROTECT otbEditCut AS System.Windows.Forms.ToolStripButton
PROTECT otbEditCopy AS System.Windows.Forms.ToolStripButton
PROTECT otbEditPaste AS System.Windows.Forms.ToolStripButton
PROTECT otbEditDelete AS System.Windows.Forms.ToolStripButton
PROTECT otbEditFind AS System.Windows.Forms.ToolStripButton
PROTECT otbEditReplace AS System.Windows.Forms.ToolStripButton
PROTECT otbEditRefresh AS System.Windows.Forms.ToolStripButton
PROTECT otbEditPreferences AS System.Windows.Forms.ToolStripButton
PROTECT otbEditProperties AS System.Windows.Forms.ToolStripButton
PROTECT otoolbarSeparator2 AS System.Windows.Forms.ToolStripSeparator
PROTECT otbHelpContents AS System.Windows.Forms.ToolStripButton
PROTECT otbHelpAbout AS System.Windows.Forms.ToolStripButton
PROTECT omenuFile AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuFileNew AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuFileOpen AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuFileSave AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuFileSaveAs AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuFileSeparator1 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuFilePrint AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuFileSeparator2 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuFileQuit AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEdit AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditUndo AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditRedo AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditSeparator1 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuEditSelectAll AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditCut AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditCopy AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditPaste AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditPasteSpecial AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditDelete AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditSeparator2 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuEditFind AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditReplace AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditSeparator3 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuEditRefresh AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditSeparator4 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuEditPreferences AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuEditProperties AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindow AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindowNewWindow AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindowTile AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindowCascade AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindowArrangeAll AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindowSeparator1 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuWindowHide AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuWindowShow AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelp AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelpContents AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelpIndex AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelpOnlineHelp AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelpSeparator1 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuHelpLicenseInformation AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelpCheckForUpdates AS System.Windows.Forms.ToolStripMenuItem
PROTECT omenuHelpSeparator2 AS System.Windows.Forms.ToolStripSeparator
PROTECT omenuHelpAbout AS System.Windows.Forms.ToolStripMenuItem
// User code starts here (DO NOT remove this line)  ##USER##

//Note:Const has the 'AS type' clause in the initializer after the literal
PRIVATE CONST ACTION_IN_PROGRESS := " ..." AS STRING
PRIVATE CONST ACTION_CANCELLED := " cancelled" AS STRING
PRIVATE CONST ACTION_DONE := " done" AS STRING


CONSTRUCTOR()

	// This has to be before TRY; 
	//  like C# it is executed before the rest of the contents of the method
	SUPER()
	
	TRY
		SELF:InitializeForm()

	CATCH ex AS Exception 
		Console.Error.WriteLine(ex:Message)
	END

RETURN

METHOD InitializeForm() AS VOID

// IDE generated code (please DO NOT modify)

	LOCAL oResourceManager AS System.Resources.ResourceManager

	oResourceManager := System.Resources.ResourceManager{ "Designers" , System.Reflection.Assembly.GetExecutingAssembly() }

	SELF:oToolTip1 := System.Windows.Forms.ToolTip{}
	SELF:olblSomeDate := System.Windows.Forms.Label{}
	SELF:odtmSomeDate := System.Windows.Forms.DateTimePicker{}
	SELF:ochkSomeBool := System.Windows.Forms.CheckBox{}
	SELF:otxtSomeString := System.Windows.Forms.TextBox{}
	SELF:otxtSomeInt := System.Windows.Forms.TextBox{}
	SELF:olblSomeBool := System.Windows.Forms.Label{}
	SELF:olblSomeString := System.Windows.Forms.Label{}
	SELF:olblSomeInt := System.Windows.Forms.Label{}
	SELF:oBtnError := System.Windows.Forms.Button{}
	SELF:obtnRun := System.Windows.Forms.Button{}
	SELF:obtnColor := System.Windows.Forms.Button{}
	SELF:obtnFont := System.Windows.Forms.Button{}
	SELF:opicDirty := System.Windows.Forms.PictureBox{}
	SELF:opicAction := System.Windows.Forms.PictureBox{}
	SELF:olblError := System.Windows.Forms.Label{}
	SELF:olblStatus := System.Windows.Forms.Label{}
	SELF:oprgProgress := System.Windows.Forms.ProgressBar{}
	SELF:oStatusBar1 := System.Windows.Forms.StatusBar{}
	SELF:oToolStrip1 := System.Windows.Forms.ToolStrip{}
	SELF:oMenuStrip1 := System.Windows.Forms.MenuStrip{}
	SELF:otbFileNew := System.Windows.Forms.ToolStripButton{}
	SELF:otbFileOpen := System.Windows.Forms.ToolStripButton{}
	SELF:otbFileSave := System.Windows.Forms.ToolStripButton{}
	SELF:otbFilePrint := System.Windows.Forms.ToolStripButton{}
	SELF:otoolbarSeparator1 := System.Windows.Forms.ToolStripSeparator{}
	SELF:otbEditUndo := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditRedo := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditCut := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditCopy := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditPaste := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditDelete := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditFind := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditReplace := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditRefresh := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditPreferences := System.Windows.Forms.ToolStripButton{}
	SELF:otbEditProperties := System.Windows.Forms.ToolStripButton{}
	SELF:otoolbarSeparator2 := System.Windows.Forms.ToolStripSeparator{}
	SELF:otbHelpContents := System.Windows.Forms.ToolStripButton{}
	SELF:otbHelpAbout := System.Windows.Forms.ToolStripButton{}
	SELF:omenuFile := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuFileNew := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuFileOpen := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuFileSave := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuFileSaveAs := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuFileSeparator1 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuFilePrint := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuFileSeparator2 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuFileQuit := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEdit := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditUndo := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditRedo := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditSeparator1 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuEditSelectAll := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditCut := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditCopy := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditPaste := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditPasteSpecial := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditDelete := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditSeparator2 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuEditFind := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditReplace := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditSeparator3 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuEditRefresh := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditSeparator4 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuEditPreferences := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuEditProperties := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindow := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindowNewWindow := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindowTile := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindowCascade := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindowArrangeAll := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindowSeparator1 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuWindowHide := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuWindowShow := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelp := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelpContents := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelpIndex := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelpOnlineHelp := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelpSeparator1 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuHelpLicenseInformation := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelpCheckForUpdates := System.Windows.Forms.ToolStripMenuItem{}
	SELF:omenuHelpSeparator2 := System.Windows.Forms.ToolStripSeparator{}
	SELF:omenuHelpAbout := System.Windows.Forms.ToolStripMenuItem{}


	SELF:SuspendLayout()

	SELF:ClientSize := System.Drawing.Size{640 , 480}
	SELF:Closing += SELF:BasicForm_Closing
	SELF:Icon := (System.Drawing.Icon)oResourceManager:GetObject( "App.ico" )
	SELF:Load += SELF:BasicForm_Load
	SELF:Location := System.Drawing.Point{0 , 0}
	SELF:MinimumSize := System.Drawing.Size{640 , 480}
	SELF:Name := "BasicForm"
	SELF:Text := "XSharp App 1"

	SELF:MainMenuStrip := SELF:oMenuStrip1

	SELF:oToolStrip1:Name := "ToolStrip1"

	SELF:otbFileNew:Click += SELF:menuFileNewClick
	SELF:otbFileNew:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "New.png" )
	SELF:otbFileNew:Name := "tbFileNew"
	SELF:otbFileNew:Text := ""
	SELF:otbFileNew:TooltipText := "New"
	SELF:oToolStrip1:Items:Add( SELF:otbFileNew)

	SELF:otbFileOpen:Click += SELF:MenuFileOpen_Click
	SELF:otbFileOpen:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Open.png" )
	SELF:otbFileOpen:Name := "tbFileOpen"
	SELF:otbFileOpen:Text := ""
	SELF:otbFileOpen:TooltipText := "Open"
	SELF:oToolStrip1:Items:Add( SELF:otbFileOpen)

	SELF:otbFileSave:Click += SELF:MenuFileSave_Click
	SELF:otbFileSave:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Save.png" )
	SELF:otbFileSave:Name := "tbFileSave"
	SELF:otbFileSave:Text := ""
	SELF:otbFileSave:TooltipText := "Save"
	SELF:oToolStrip1:Items:Add( SELF:otbFileSave)

	SELF:otbFilePrint:Click += SELF:MenuFilePrint_Click
	SELF:otbFilePrint:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Print.png" )
	SELF:otbFilePrint:Name := "tbFilePrint"
	SELF:otbFilePrint:Text := ""
	SELF:otbFilePrint:TooltipText := "Print"
	SELF:oToolStrip1:Items:Add( SELF:otbFilePrint)

	SELF:otoolbarSeparator1:Name := "toolbarSeparator1"
	SELF:otoolbarSeparator1:Text := "-"
	SELF:oToolStrip1:Items:Add( SELF:otoolbarSeparator1)

	SELF:otbEditUndo:Click += SELF:MenuEditUndo_Click
	SELF:otbEditUndo:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Undo.png" )
	SELF:otbEditUndo:Name := "tbEditUndo"
	SELF:otbEditUndo:Text := ""
	SELF:otbEditUndo:TooltipText := "Undo"
	SELF:oToolStrip1:Items:Add( SELF:otbEditUndo)

	SELF:otbEditRedo:Click += SELF:MenuEditRedo_Click
	SELF:otbEditRedo:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Redo.png" )
	SELF:otbEditRedo:Name := "tbEditRedo"
	SELF:otbEditRedo:Text := ""
	SELF:otbEditRedo:TooltipText := "Redo"
	SELF:oToolStrip1:Items:Add( SELF:otbEditRedo)

	SELF:otbEditCut:Click += SELF:MenuEditCut_Click
	SELF:otbEditCut:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Cut.png" )
	SELF:otbEditCut:Name := "tbEditCut"
	SELF:otbEditCut:Text := ""
	SELF:otbEditCut:TooltipText := "Cut"
	SELF:oToolStrip1:Items:Add( SELF:otbEditCut)

	SELF:otbEditCopy:Click += SELF:MenuEditCopy_Click
	SELF:otbEditCopy:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Copy.png" )
	SELF:otbEditCopy:Name := "tbEditCopy"
	SELF:otbEditCopy:Text := ""
	SELF:otbEditCopy:TooltipText := "Copy"
	SELF:oToolStrip1:Items:Add( SELF:otbEditCopy)

	SELF:otbEditPaste:Click += SELF:MenuEditPaste_Click
	SELF:otbEditPaste:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Paste.png" )
	SELF:otbEditPaste:Name := "tbEditPaste"
	SELF:otbEditPaste:Text := ""
	SELF:otbEditPaste:TooltipText := "Paste"
	SELF:oToolStrip1:Items:Add( SELF:otbEditPaste)

	SELF:otbEditDelete:Click += SELF:MenuEditDelete_Click
	SELF:otbEditDelete:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Delete.png" )
	SELF:otbEditDelete:Name := "tbEditDelete"
	SELF:otbEditDelete:Text := ""
	SELF:otbEditDelete:TooltipText := "Delete"
	SELF:oToolStrip1:Items:Add( SELF:otbEditDelete)

	SELF:otbEditFind:Click += SELF:MenuEditFind_Click
	SELF:otbEditFind:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Find.png" )
	SELF:otbEditFind:Name := "tbEditFind"
	SELF:otbEditFind:Text := ""
	SELF:otbEditFind:TooltipText := "Find"
	SELF:oToolStrip1:Items:Add( SELF:otbEditFind)

	SELF:otbEditReplace:Click += SELF:MenuEditReplace_Click
	SELF:otbEditReplace:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Replace.png" )
	SELF:otbEditReplace:Name := "tbEditReplace"
	SELF:otbEditReplace:Text := ""
	SELF:otbEditReplace:TooltipText := "Replace"
	SELF:oToolStrip1:Items:Add( SELF:otbEditReplace)

	SELF:otbEditRefresh:Click += SELF:MenuEditRefresh_Click
	SELF:otbEditRefresh:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Reload.png" )
	SELF:otbEditRefresh:Name := "tbEditRefresh"
	SELF:otbEditRefresh:Text := ""
	SELF:otbEditRefresh:TooltipText := "Refresh"
	SELF:oToolStrip1:Items:Add( SELF:otbEditRefresh)

	SELF:otbEditPreferences:Click += SELF:MenuEditPreferences_Click
	SELF:otbEditPreferences:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Preferences.png" )
	SELF:otbEditPreferences:Name := "tbEditPreferences"
	SELF:otbEditPreferences:Text := ""
	SELF:otbEditPreferences:TooltipText := "Preferences"
	SELF:oToolStrip1:Items:Add( SELF:otbEditPreferences)

	SELF:otbEditProperties:Click += SELF:MenuEditProperties_Click
	SELF:otbEditProperties:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Properties.png" )
	SELF:otbEditProperties:Name := "tbEditProperties"
	SELF:otbEditProperties:Text := ""
	SELF:otbEditProperties:TooltipText := "Properties"
	SELF:oToolStrip1:Items:Add( SELF:otbEditProperties)

	SELF:otoolbarSeparator2:Name := "toolbarSeparator2"
	SELF:otoolbarSeparator2:Text := "-"
	SELF:oToolStrip1:Items:Add( SELF:otoolbarSeparator2)

	SELF:otbHelpContents:Click += SELF:MenuHelpContents_Click
	SELF:otbHelpContents:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Contents.png" )
	SELF:otbHelpContents:Name := "tbHelpContents"
	SELF:otbHelpContents:Text := ""
	SELF:otbHelpContents:TooltipText := "Contents"
	SELF:oToolStrip1:Items:Add( SELF:otbHelpContents)

	SELF:otbHelpAbout:Click += SELF:MenuHelpAbout_Click
	SELF:otbHelpAbout:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "About.png" )
	SELF:otbHelpAbout:Name := "tbHelpAbout"
	SELF:otbHelpAbout:Text := ""
	SELF:otbHelpAbout:TooltipText := "About"
	SELF:oToolStrip1:Items:Add( SELF:otbHelpAbout)

	SELF:oMenuStrip1:Name := "MenuStrip1"

	SELF:omenuFile:Name := "menuFile"
	SELF:omenuFile:ShortcutKeys := System.Windows.Forms.Keys.N + System.Windows.Forms.Keys.Control
	SELF:omenuFile:Text := "&File"
	SELF:oMenuStrip1:Items:Add( SELF:omenuFile)

	SELF:omenuFileNew:Click += SELF:menuFileNewClick
	SELF:omenuFileNew:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "New.png" )
	SELF:omenuFileNew:Name := "menuFileNew"
	SELF:omenuFileNew:ShortcutKeys := System.Windows.Forms.Keys.N + System.Windows.Forms.Keys.Control
	SELF:omenuFileNew:Text := "&New"
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileNew)

	SELF:omenuFileOpen:Click += SELF:MenuFileOpen_Click
	SELF:omenuFileOpen:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Open.png" )
	SELF:omenuFileOpen:Name := "menuFileOpen"
	SELF:omenuFileOpen:ShortcutKeys := System.Windows.Forms.Keys.O + System.Windows.Forms.Keys.Control
	SELF:omenuFileOpen:Text := "&Open..."
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileOpen)

	SELF:omenuFileSave:Click += SELF:MenuFileSave_Click
	SELF:omenuFileSave:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Save.png" )
	SELF:omenuFileSave:Name := "menuFileSave"
	SELF:omenuFileSave:ShortcutKeys := System.Windows.Forms.Keys.S + System.Windows.Forms.Keys.Control
	SELF:omenuFileSave:Text := "&Save"
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileSave)

	SELF:omenuFileSaveAs:Click += SELF:MenuFileSaveAs_Click
	SELF:omenuFileSaveAs:Name := "menuFileSaveAs"
	SELF:omenuFileSaveAs:Text := "Save &As..."
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileSaveAs)

	SELF:omenuFileSeparator1:Name := "menuFileSeparator1"
	SELF:omenuFileSeparator1:Text := "-"
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileSeparator1)

	SELF:omenuFilePrint:Click += SELF:MenuFilePrint_Click
	SELF:omenuFilePrint:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Print.png" )
	SELF:omenuFilePrint:Name := "menuFilePrint"
	SELF:omenuFilePrint:ShortcutKeys := System.Windows.Forms.Keys.P + System.Windows.Forms.Keys.Control
	SELF:omenuFilePrint:Text := "&Print"
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFilePrint)

	SELF:omenuFileSeparator2:Name := "menuFileSeparator2"
	SELF:omenuFileSeparator2:Text := "-"
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileSeparator2)

	SELF:omenuFileQuit:Click += SELF:menuFileQuit_Click
	SELF:omenuFileQuit:Name := "menuFileQuit"
	SELF:omenuFileQuit:ShortcutKeys := System.Windows.Forms.Keys.Q + System.Windows.Forms.Keys.Control
	SELF:omenuFileQuit:Text := "&Quit"
	SELF:omenuFile:DropDown:Items:Add( SELF:omenuFileQuit)

	SELF:omenuEdit:Name := "menuEdit"
	SELF:omenuEdit:Text := "&Edit"
	SELF:oMenuStrip1:Items:Add( SELF:omenuEdit)

	SELF:omenuEditUndo:Click += SELF:MenuEditUndo_Click
	SELF:omenuEditUndo:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Undo.png" )
	SELF:omenuEditUndo:Name := "menuEditUndo"
	SELF:omenuEditUndo:ShortcutKeys := System.Windows.Forms.Keys.Z + System.Windows.Forms.Keys.Control
	SELF:omenuEditUndo:Text := "&Undo"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditUndo)

	SELF:omenuEditRedo:Click += SELF:MenuEditRedo_Click
	SELF:omenuEditRedo:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Redo.png" )
	SELF:omenuEditRedo:Name := "menuEditRedo"
	SELF:omenuEditRedo:ShortcutKeys := System.Windows.Forms.Keys.Y + System.Windows.Forms.Keys.Control
	SELF:omenuEditRedo:Text := "&Redo"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditRedo)

	SELF:omenuEditSeparator1:Name := "menuEditSeparator1"
	SELF:omenuEditSeparator1:Text := "-"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditSeparator1)

	SELF:omenuEditSelectAll:Click += SELF:MenuEditSelectAll_Click
	SELF:omenuEditSelectAll:Name := "menuEditSelectAll"
	SELF:omenuEditSelectAll:ShortcutKeys := System.Windows.Forms.Keys.A + System.Windows.Forms.Keys.Control
	SELF:omenuEditSelectAll:Text := "Select &All"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditSelectAll)

	SELF:omenuEditCut:Click += SELF:MenuEditCut_Click
	SELF:omenuEditCut:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Cut.png" )
	SELF:omenuEditCut:Name := "menuEditCut"
	SELF:omenuEditCut:ShortcutKeys := System.Windows.Forms.Keys.X + System.Windows.Forms.Keys.Control
	SELF:omenuEditCut:Text := "Cu&t"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditCut)

	SELF:omenuEditCopy:Click += SELF:MenuEditCopy_Click
	SELF:omenuEditCopy:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Copy.png" )
	SELF:omenuEditCopy:Name := "menuEditCopy"
	SELF:omenuEditCopy:ShortcutKeys := System.Windows.Forms.Keys.C + System.Windows.Forms.Keys.Control
	SELF:omenuEditCopy:Text := "C&opy"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditCopy)

	SELF:omenuEditPaste:Click += SELF:MenuEditPaste_Click
	SELF:omenuEditPaste:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Paste.png" )
	SELF:omenuEditPaste:Name := "menuEditPaste"
	SELF:omenuEditPaste:ShortcutKeys := System.Windows.Forms.Keys.V + System.Windows.Forms.Keys.Control
	SELF:omenuEditPaste:Text := "&Paste"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditPaste)

	SELF:omenuEditPasteSpecial:Click += SELF:MenuEditPasteSpecial_Click
	SELF:omenuEditPasteSpecial:Name := "menuEditPasteSpecial"
	SELF:omenuEditPasteSpecial:Text := "Paste &Special..."
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditPasteSpecial)

	SELF:omenuEditDelete:Click += SELF:MenuEditDelete_Click
	SELF:omenuEditDelete:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Delete.png" )
	SELF:omenuEditDelete:Name := "menuEditDelete"
	SELF:omenuEditDelete:Text := "&Delete"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditDelete)

	SELF:omenuEditSeparator2:Name := "menuEditSeparator2"
	SELF:omenuEditSeparator2:Text := "-"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditSeparator2)

	SELF:omenuEditFind:Click += SELF:MenuEditFind_Click
	SELF:omenuEditFind:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Find.png" )
	SELF:omenuEditFind:Name := "menuEditFind"
	SELF:omenuEditFind:ShortcutKeys := System.Windows.Forms.Keys.F + System.Windows.Forms.Keys.Control
	SELF:omenuEditFind:Text := "F&ind"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditFind)

	SELF:omenuEditReplace:Click += SELF:MenuEditReplace_Click
	SELF:omenuEditReplace:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Replace.png" )
	SELF:omenuEditReplace:Name := "menuEditReplace"
	SELF:omenuEditReplace:ShortcutKeys := System.Windows.Forms.Keys.H + System.Windows.Forms.Keys.Control
	SELF:omenuEditReplace:Text := "R&eplace"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditReplace)

	SELF:omenuEditSeparator3:Name := "menuEditSeparator3"
	SELF:omenuEditSeparator3:Text := "-"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditSeparator3)

	SELF:omenuEditRefresh:Click += SELF:MenuEditRefresh_Click
	SELF:omenuEditRefresh:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Reload.png" )
	SELF:omenuEditRefresh:Name := "menuEditRefresh"
	SELF:omenuEditRefresh:ShortcutKeys := System.Windows.Forms.Keys.F5
	SELF:omenuEditRefresh:Text := "Re&fresh"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditRefresh)

	SELF:omenuEditSeparator4:Name := "menuEditSeparator4"
	SELF:omenuEditSeparator4:Text := "-"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditSeparator4)

	SELF:omenuEditPreferences:Click += SELF:MenuEditPreferences_Click
	SELF:omenuEditPreferences:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Preferences.png" )
	SELF:omenuEditPreferences:Name := "menuEditPreferences"
	SELF:omenuEditPreferences:Text := "Prefere&nces"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditPreferences)

	SELF:omenuEditProperties:Click += SELF:MenuEditProperties_Click
	SELF:omenuEditProperties:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Properties.png" )
	SELF:omenuEditProperties:Name := "menuEditProperties"
	SELF:omenuEditProperties:Text := "P&roperties"
	SELF:omenuEdit:DropDown:Items:Add( SELF:omenuEditProperties)

	SELF:omenuWindow:Name := "menuWindow"
	SELF:omenuWindow:Text := "&Window"
	SELF:oMenuStrip1:Items:Add( SELF:omenuWindow)

	SELF:omenuWindowNewWindow:Click += SELF:MenuWindowNewWindow_Click
	SELF:omenuWindowNewWindow:Name := "menuWindowNewWindow"
	SELF:omenuWindowNewWindow:Text := "&New Window"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowNewWindow)

	SELF:omenuWindowTile:Click += SELF:MenuWindowTile_Click
	SELF:omenuWindowTile:Name := "menuWindowTile"
	SELF:omenuWindowTile:Text := "&Tile"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowTile)

	SELF:omenuWindowCascade:Click += SELF:MenuWindowCascade_Click
	SELF:omenuWindowCascade:Name := "menuWindowCascade"
	SELF:omenuWindowCascade:Text := "&Cascade"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowCascade)

	SELF:omenuWindowArrangeAll:Click += SELF:MenuWindowArrangeAll_Click
	SELF:omenuWindowArrangeAll:Name := "menuWindowArrangeAll"
	SELF:omenuWindowArrangeAll:Text := "&Arrange All"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowArrangeAll)

	SELF:omenuWindowSeparator1:Name := "menuWindowSeparator1"
	SELF:omenuWindowSeparator1:Text := "-"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowSeparator1)

	SELF:omenuWindowHide:Click += SELF:MenuWindowHide_Click
	SELF:omenuWindowHide:Name := "menuWindowHide"
	SELF:omenuWindowHide:Text := "&Hide"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowHide)

	SELF:omenuWindowShow:Click += SELF:MenuWindowShow_Click
	SELF:omenuWindowShow:Name := "menuWindowShow"
	SELF:omenuWindowShow:Text := "&Show"
	SELF:omenuWindow:DropDown:Items:Add( SELF:omenuWindowShow)

	SELF:omenuHelp:Name := "menuHelp"
	SELF:omenuHelp:Text := "&Help"
	SELF:oMenuStrip1:Items:Add( SELF:omenuHelp)

	SELF:omenuHelpContents:Click += SELF:MenuHelpContents_Click
	SELF:omenuHelpContents:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Contents.png" )
	SELF:omenuHelpContents:Name := "menuHelpContents"
	SELF:omenuHelpContents:ShortcutKeys := System.Windows.Forms.Keys.F1
	SELF:omenuHelpContents:Text := "&Contents"
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpContents)

	SELF:omenuHelpIndex:Click += SELF:MenuHelpIndex_Click
	SELF:omenuHelpIndex:Name := "menuHelpIndex"
	SELF:omenuHelpIndex:Text := "&Index"
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpIndex)

	SELF:omenuHelpOnlineHelp:Click += SELF:MenuHelpOnlineHelp_Click
	SELF:omenuHelpOnlineHelp:Name := "menuHelpOnlineHelp"
	SELF:omenuHelpOnlineHelp:Text := "&Online Help"
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpOnlineHelp)

	SELF:omenuHelpSeparator1:Name := "menuHelpSeparator1"
	SELF:omenuHelpSeparator1:Text := "-"
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpSeparator1)

	SELF:omenuHelpLicenseInformation:Click += SELF:MenuHelpLicenceInformation_Click
	SELF:omenuHelpLicenseInformation:Name := "menuHelpLicenseInformation"
	SELF:omenuHelpLicenseInformation:Text := "&License Information"
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpLicenseInformation)

	SELF:omenuHelpCheckForUpdates:Click += SELF:MenuHelpCheckForUpdates_Click
	SELF:omenuHelpCheckForUpdates:Name := "menuHelpCheckForUpdates"
	SELF:omenuHelpCheckForUpdates:Text := "Check for &Updates..."
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpCheckForUpdates)

	SELF:omenuHelpSeparator2:Name := "menuHelpSeparator2"
	SELF:omenuHelpSeparator2:Text := "-"
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpSeparator2)

	SELF:omenuHelpAbout:Click += SELF:MenuHelpAbout_Click
	SELF:omenuHelpAbout:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "About.png" )
	SELF:omenuHelpAbout:Name := "menuHelpAbout"
	SELF:omenuHelpAbout:Text := "&About..."
	SELF:omenuHelp:DropDown:Items:Add( SELF:omenuHelpAbout)
	SELF:olblSomeDate:Location := System.Drawing.Point{8 , 128}
	SELF:olblSomeDate:Name := "lblSomeDate"
	SELF:olblSomeDate:Size := System.Drawing.Size{88 , 23}
	SELF:olblSomeDate:TabIndex := 19
	SELF:olblSomeDate:Text := "Some Date:"
	SELF:Controls:Add(SELF:olblSomeDate)
	
	SELF:odtmSomeDate:Location := System.Drawing.Point{104 , 128}
	SELF:odtmSomeDate:Name := "dtmSomeDate"
	SELF:odtmSomeDate:Size := System.Drawing.Size{192 , 20}
	SELF:odtmSomeDate:TabIndex := 18
	SELF:Controls:Add(SELF:odtmSomeDate)
	
	SELF:ochkSomeBool:Location := System.Drawing.Point{104 , 104}
	SELF:ochkSomeBool:Name := "chkSomeBool"
	SELF:ochkSomeBool:Size := System.Drawing.Size{104 , 24}
	SELF:ochkSomeBool:TabIndex := 17
	SELF:Controls:Add(SELF:ochkSomeBool)
	
	SELF:otxtSomeString:Location := System.Drawing.Point{104 , 80}
	SELF:otxtSomeString:Name := "txtSomeString"
	SELF:otxtSomeString:Size := System.Drawing.Size{192 , 20}
	SELF:otxtSomeString:TabIndex := 16
	SELF:Controls:Add(SELF:otxtSomeString)
	
	SELF:otxtSomeInt:Location := System.Drawing.Point{104 , 56}
	SELF:otxtSomeInt:Name := "txtSomeInt"
	SELF:otxtSomeInt:Size := System.Drawing.Size{100 , 20}
	SELF:otxtSomeInt:TabIndex := 15
	SELF:otxtSomeInt:Text := "0"
	SELF:Controls:Add(SELF:otxtSomeInt)
	
	SELF:olblSomeBool:Location := System.Drawing.Point{8 , 104}
	SELF:olblSomeBool:Name := "lblSomeBool"
	SELF:olblSomeBool:Size := System.Drawing.Size{88 , 23}
	SELF:olblSomeBool:TabIndex := 14
	SELF:olblSomeBool:Text := "Some Boolean:"
	SELF:Controls:Add(SELF:olblSomeBool)
	
	SELF:olblSomeString:Location := System.Drawing.Point{8 , 80}
	SELF:olblSomeString:Name := "lblSomeString"
	SELF:olblSomeString:Size := System.Drawing.Size{88 , 23}
	SELF:olblSomeString:TabIndex := 13
	SELF:olblSomeString:Text := "Some String:"
	SELF:Controls:Add(SELF:olblSomeString)
	
	SELF:olblSomeInt:Location := System.Drawing.Point{8 , 56}
	SELF:olblSomeInt:Name := "lblSomeInt"
	SELF:olblSomeInt:Size := System.Drawing.Size{88 , 23}
	SELF:olblSomeInt:TabIndex := 12
	SELF:olblSomeInt:Text := "Some Integer:"
	SELF:Controls:Add(SELF:olblSomeInt)
	
	SELF:oBtnError:Click += SELF:cmdError_Click
	SELF:oBtnError:Location := System.Drawing.Point{544 , 136}
	SELF:oBtnError:Name := "BtnError"
	SELF:oBtnError:Size := System.Drawing.Size{75 , 23}
	SELF:oBtnError:TabIndex := 11
	SELF:oBtnError:Text := "Error"
	SELF:Controls:Add(SELF:oBtnError)
	
	SELF:obtnRun:Click += SELF:cmdRun_Click
	SELF:obtnRun:Location := System.Drawing.Point{544 , 104}
	SELF:obtnRun:Name := "btnRun"
	SELF:obtnRun:Size := System.Drawing.Size{75 , 23}
	SELF:obtnRun:TabIndex := 10
	SELF:obtnRun:Text := "Run"
	SELF:Controls:Add(SELF:obtnRun)
	
	SELF:obtnColor:Click += SELF:cmdColor_Click
	SELF:obtnColor:Location := System.Drawing.Point{544 , 352}
	SELF:obtnColor:Name := "btnColor"
	SELF:obtnColor:Size := System.Drawing.Size{75 , 23}
	SELF:obtnColor:TabIndex := 9
	SELF:obtnColor:Text := "Color"
	SELF:Controls:Add(SELF:obtnColor)
	
	SELF:obtnFont:Click += SELF:cmdFont_Click
	SELF:obtnFont:Location := System.Drawing.Point{544 , 320}
	SELF:obtnFont:Name := "btnFont"
	SELF:obtnFont:Size := System.Drawing.Size{75 , 23}
	SELF:obtnFont:TabIndex := 8
	SELF:obtnFont:Text := "Font"
	SELF:Controls:Add(SELF:obtnFont)
	
	SELF:opicDirty:BorderStyle := System.Windows.Forms.BorderStyle.Fixed3D
	SELF:opicDirty:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "Save.png" )
	SELF:opicDirty:Location := System.Drawing.Point{600 , 456}
	SELF:opicDirty:Name := "picDirty"
	SELF:opicDirty:Size := System.Drawing.Size{22 , 22}
	SELF:opicDirty:Visible := FALSE
	SELF:Controls:Add(SELF:opicDirty)
	
	SELF:opicAction:BorderStyle := System.Windows.Forms.BorderStyle.Fixed3D
	SELF:opicAction:Image := (System.Drawing.Bitmap)oResourceManager:GetObject( "New.png" )
	SELF:opicAction:Location := System.Drawing.Point{576 , 456}
	SELF:opicAction:Name := "picAction"
	SELF:opicAction:Size := System.Drawing.Size{22 , 22}
	SELF:opicAction:Visible := FALSE
	SELF:Controls:Add(SELF:opicAction)
	
	SELF:olblError:Anchor := System.Windows.Forms.AnchorStyles.Left + System.Windows.Forms.AnchorStyles.Right + System.Windows.Forms.AnchorStyles.Bottom
	SELF:olblError:BorderStyle := System.Windows.Forms.BorderStyle.Fixed3D
	SELF:olblError:ForeColor := System.Drawing.Color.FromArgb( 213,0,0 )
	SELF:olblError:Location := System.Drawing.Point{264 , 456}
	SELF:olblError:Name := "lblError"
	SELF:olblError:Size := System.Drawing.Size{208 , 22}
	SELF:olblError:TabIndex := 6
	SELF:Controls:Add(SELF:olblError)
	
	SELF:olblStatus:Anchor := System.Windows.Forms.AnchorStyles.Left + System.Windows.Forms.AnchorStyles.Bottom
	SELF:olblStatus:BorderStyle := System.Windows.Forms.BorderStyle.Fixed3D
	SELF:olblStatus:ForeColor := System.Drawing.Color.FromArgb( 0,128,0 )
	SELF:olblStatus:Location := System.Drawing.Point{0 , 456}
	SELF:olblStatus:Name := "lblStatus"
	SELF:olblStatus:Size := System.Drawing.Size{256 , 22}
	SELF:olblStatus:TabIndex := 5
	SELF:Controls:Add(SELF:olblStatus)
	
	SELF:oprgProgress:Anchor := System.Windows.Forms.AnchorStyles.Right + System.Windows.Forms.AnchorStyles.Bottom
	SELF:oprgProgress:Location := System.Drawing.Point{480 , 456}
	SELF:oprgProgress:Name := "prgProgress"
	SELF:oprgProgress:Size := System.Drawing.Size{88 , 22}
	SELF:oprgProgress:Style := System.Windows.Forms.ProgressBarStyle.Marquee
	SELF:oprgProgress:TabIndex := 4
	SELF:oprgProgress:Value := 33
	SELF:oprgProgress:Visible := FALSE
	SELF:Controls:Add(SELF:oprgProgress)
	
	SELF:oStatusBar1:Location := System.Drawing.Point{0 , 455}
	SELF:oStatusBar1:Name := "StatusBar1"
	SELF:oStatusBar1:Size := System.Drawing.Size{640 , 25}
	SELF:oStatusBar1:TabIndex := 1
	SELF:Controls:Add(SELF:oStatusBar1)
	
	SELF:oToolStrip1:Location := System.Drawing.Point{0 , 24}
	SELF:oToolStrip1:Name := "ToolStrip1"
	SELF:oToolStrip1:Size := System.Drawing.Size{640 , 25}
	SELF:oToolStrip1:TabIndex := 2
	SELF:Controls:Add(SELF:oToolStrip1)
	
	SELF:oMenuStrip1:Location := System.Drawing.Point{0 , 0}
	SELF:oMenuStrip1:Name := "MenuStrip1"
	SELF:oMenuStrip1:Size := System.Drawing.Size{640 , 24}
	SELF:oMenuStrip1:TabIndex := 3
	SELF:Controls:Add(SELF:oMenuStrip1)
	
	SELF:ResumeLayout()

RETURN

METHOD placeholder() AS VOID
	// Note: if this method is not present, changes to the form designer
	//   will erase the #region tags below when the changes are saved.
	// The designer respects the presence of keywords, 
	//  but not pre-processor directives.
RETURN

#region Events
#region Form Events
METHOD BasicForm_Load(sender AS System.Object , e AS System.EventArgs) AS VOID
	TRY
		Console.WriteLine("BasicForm_Load")
	CATCH ex AS Exception 
		Console.Error.WriteLine(ex:Message)
	END
RETURN

METHOD BasicForm_Closing(sender AS System.Object , e AS System.ComponentModel.CancelEventArgs) AS VOID
	LOCAL resultCancel AS Boolean
	
	TRY
		resultCancel := FALSE

		Console.WriteLine("BasicForm_Closing")
		SetError("", FALSE) 
		SetStatus("Quit" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		//StartActionIcon((Bitmap)omenuFileQuit.Image)

		//prompt user before quit
		FileQuitAction(REF resultCancel)
		((System.ComponentModel.CancelEventArgs)e).Cancel := resultCancel
		
		IF (resultCancel)
			SetStatus(ACTION_CANCELLED, TRUE) 
		ELSE
			SetStatus(ACTION_DONE, TRUE)  
		ENDIF

		//StopActionIcon()
		StopProgressBar()
	CATCH ex AS Exception 
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
#endregion Form Events

#region Menu Events
ASYNC METHOD menuFileNewClick(sender AS System.Object , e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("New" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuFileNew.Image)
		AWAIT FileNewAction()
		StopActionIcon()
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE)  
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN

/*ASYNC*/ METHOD MenuFileOpen_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Open" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuFileOpen.Image) 
		/*AWAIT*/ FileOpenAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
/*async*/ METHOD 	MenuFileSave_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Save" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuFileSave.Image) 
		/*AWAIT*/ FileSaveAction(FALSE)
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
/*async*/ METHOD 	MenuFileSaveAs_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Save As" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("SaveAs") 
		/*AWAIT*/ FileSaveAsAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
/*async*/ METHOD 	MenuFilePrint_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Print" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuFilePrint.Image) 
		/*AWAIT*/ FilePrintAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
/*async*/ METHOD 	MenuFilePrintPreview_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Print Preview" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("PrintPreview") 
		/*AWAIT*/ FilePrintPreviewAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
METHOD menuFileQuit_Click(sender AS System.Object , e AS System.EventArgs) AS VOID
	TRY
		SELF:Close()
	CATCH ex AS Exception 
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
ASYNC METHOD MenuEditUndo_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Undo" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditUndo.Image) 
		AWAIT EditUndoAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditRedo_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Redo" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditRedo.Image) 
		AWAIT EditRedoAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditSelectAll_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Select All" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("SelectAll") 
		AWAIT EditSelectAllAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditCut_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Cut" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditCut.Image) 
		AWAIT EditCutAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditCopy_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Copy" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditCopy.Image) 
		AWAIT EditCopyAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditPaste_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Paste" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditPaste.Image) 
		AWAIT EditPasteAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditPasteSpecial_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Paste Special" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("PasteSpecial") 
		AWAIT EditPasteSpecialAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditDelete_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Delete" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditDelete.Image) 
		AWAIT EditDeleteAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditFind_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Find" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditFind.Image) 
		AWAIT EditFindAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditReplace_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Replace" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditReplace.Image) 
		AWAIT EditReplaceAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditRefresh_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Refresh" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditRefresh.Image) 
		AWAIT EditRefreshAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
/*async*/ METHOD 	MenuEditPreferences_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Preferences" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditPreferences.Image) 
		/*AWAIT*/ EditPreferencesAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuEditProperties_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Properties" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuEditProperties.Image) 
		AWAIT EditPropertiesAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuWindowNewWindow_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("New Window" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("NewWindow") 
		AWAIT WindowNewWindowAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuWindowTile_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Tile" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("Tile") 
		AWAIT WindowTileAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuWindowCascade_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Cascade" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("Cascade") 
		AWAIT WindowCascadeAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuWindowArrangeAll_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Arrange All" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("ArrangeAll") 
		AWAIT WindowArrangeAllAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuWindowHide_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Hide" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("Hide") 
		AWAIT WindowHideAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuWindowShow_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Show" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("Show") 
		AWAIT WindowShowAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuHelpContents_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Contents" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuHelpContents.Image) 
		AWAIT HelpContentsAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuHelpIndex_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Index" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("Index") 
		AWAIT HelpIndexAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuHelpOnlineHelp_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Online Help" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("OnlineHelp") 
		AWAIT HelpOnlineHelpAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuHelpLicenceInformation_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Licence Information" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("LicenceInformation") 
		AWAIT HelpLicenceInformationAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
ASYNC METHOD MenuHelpCheckForUpdates_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Check For Updates" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		// StartActionIcon("CheckForUpdates") 
		AWAIT HelpCheckForUpdatesAction()
		// StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
/*async*/ METHOD 	MenuHelpAbout_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("About" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
		StartActionIcon((Bitmap)omenuHelpAbout.Image) 
		/*AWAIT*/ HelpAboutAction()
		StopActionIcon() 
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN	
#endregion Menu Events

#region Toolbar Events
//Note:the toolbar will call the corresponding menu events instead
#endregion Toolbar Events
PRIVATE ASYNC METHOD cmdRun_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Do Something" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
//		 StartActionIcon("")
		//AWAIT DoSomething2()
		RunAction()
//		 StopActionIcon()
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN  
PRIVATE METHOD cmdError_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		THROW Exception{"Testing123"}
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD cmdColor_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Color" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
//		 StartActionIcon("Color")
		/*AWAIT*/ ColorAction()
//		 StopActionIcon()
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD cmdFont_Click(sender AS System.Object, e AS System.EventArgs) AS VOID
	TRY
		SetError("", FALSE) 
		SetStatus("Font" + ACTION_IN_PROGRESS, FALSE) 
		StartProgressBar()
//		 StartActionIcon("Font")
		/*AWAIT*/ FontAction()
//		 StopActionIcon()
		StopProgressBar()
		SetStatus(ACTION_DONE, TRUE) 
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
#region Control Events

#endregion Control Events
#endregion Events

#region Methods
#region Actions
PRIVATE /*ASync*/ METHOD DoSomething() AS VOID/*Task*/
	TRY
		FOR VAR i := 0 TO 1
			//DoEvents
			/*AWAIT*/ Task.Delay(1000) 
		NEXT
	CATCH ex AS Exception 
		Console.Error.WriteLine(ex:Message)
	END
RETURN 
PRIVATE ASYNC METHOD DoSomething2() AS Task
	TRY
		FOR VAR i := 0 TO 1
			//DoEvents
			AWAIT Task.Delay(1000) 
		NEXT
	CATCH ex AS Exception 
		Console.Error.WriteLine(ex:Message)
	END
RETURN 


PRIVATE ASYNC METHOD FileNewAction() AS Task
	TRY
		//AWAIT DoSomething2() 
		SELF:otxtSomeInt:Text := 0:ToString()
		SELF:otxtSomeString:Text := ""
		SELF:ochkSomeBool:Checked := FALSE
		SELF:odtmSomeDate:Value := DateTime.Now
	CATCH ex AS Exception 
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASYNC*/ METHOD FileOpenAction()  AS VOID/*Task*/
	LOCAL openFileDialog AS OpenFileDialog
	LOCAL response AS DialogResult

	TRY
		response := DialogResult.None
		openFileDialog := OpenFileDialog{}
		openFileDialog.Multiselect := FALSE

		response := openFileDialog.ShowDialog(SELF) 
		
		IF (response == DialogResult.OK)
			SetStatus(openFileDialog.FileName + ACTION_IN_PROGRESS, TRUE) 
		ELSEIF (response == DialogResult.Cancel)
			SetStatus(ACTION_CANCELLED + ACTION_IN_PROGRESS, TRUE)  //caller will complete message
		ENDIF
	
		openFileDialog.Dispose()
	CATCH ex AS Exception
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD FileSaveAction(isSaveAs AS Boolean) AS VOID/*Task*/
	LOCAL saveFileDialog AS SaveFileDialog 
	LOCAL response AS DialogResult

	TRY						   
		response := DialogResult.None
		saveFileDialog := SaveFileDialog{}
		saveFileDialog:Title := IIF(isSaveAs , "Save As..." , "Save...")

		response := saveFileDialog.ShowDialog(SELF) 
		
		IF (response == DialogResult.OK)
			SetStatus(saveFileDialog.FileName + ACTION_IN_PROGRESS, TRUE) 
		ELSEIF (response == DialogResult.Cancel)
			SetStatus(ACTION_CANCELLED + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
	
		saveFileDialog.Dispose()
	CATCH ex AS Exception
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD FileSaveAsAction() AS VOID/*Task*/
	/*AWAIT*/ FileSaveAction(TRUE)
RETURN
PRIVATE /*ASync*/ METHOD FilePrintAction() AS VOID/*Task*/
	LOCAL errorMessage AS STRING 
	LOCAL printer AS PrinterSettings
	TRY 
		errorMessage := ""
		printer := NULL
		IF (GetPrinter(REF printer, REF errorMessage))
			SetStatus(printer.PrinterName, TRUE) 
			Console.WriteLine("PrintSettings.PrinterName:" + printer.PrinterName)
			// Note: there is no actual output from printer.PrinterName
		ENDIF
	CATCH ex AS Exception
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD FilePrintPreviewAction() AS VOID/*Task*/
	/*AWAIT*/ DoSomething()
RETURN
 
PRIVATE METHOD FileQuitAction(resultCancel REF Boolean) AS VOID
	LOCAL response AS DialogResult
	
	TRY						  
		response := MessageBox.Show(SELF, "Quit?", SELF:Text, MessageBoxButtons.YesNo ,MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
		
		IF response = DialogResult.No
			resultCancel := TRUE
		ELSE
			resultCancel := FALSE
			//clean up data model here
		ENDIF 
	CATCH ex AS Exception 
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN  
PRIVATE ASYNC METHOD EditUndoAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditRedoAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditSelectAllAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditCutAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditCopyAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditPasteAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditPasteSpecialAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditDeleteAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditFindAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditReplaceAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD EditRefreshAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE /*ASync*/ METHOD EditPreferencesAction() AS VOID/*Task*/
	//do a folder-path dialog demo for preferences
	LOCAL folderBrowserDialog AS FolderBrowserDialog 
	LOCAL response AS DialogResult

	TRY   
		response := DialogResult.None
		folderBrowserDialog := FolderBrowserDialog{}
		// folderBrowserDialog.Title := this.Title;

		response := folderBrowserDialog.ShowDialog(SELF) 
		
		IF (response == DialogResult.OK)
			SetStatus(folderBrowserDialog.SelectedPath + ACTION_IN_PROGRESS, TRUE) 
		ELSE //if (response == DialogResult.Cancel)
			SetStatus(ACTION_CANCELLED + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
	
		folderBrowserDialog.Dispose()
	CATCH ex AS Exception
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE ASYNC METHOD EditPropertiesAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD WindowNewWindowAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD WindowTileAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD WindowCascadeAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD WindowArrangeAllAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD WindowHideAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD WindowShowAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD HelpContentsAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD HelpIndexAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD HelpOnlineHelpAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD HelpLicenceInformationAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE ASYNC METHOD HelpCheckForUpdatesAction() AS Task
	AWAIT DoSomething2()
RETURN
PRIVATE /*ASync*/ METHOD HelpAboutAction() AS VOID/*Task*/
	LOCAL response AS DialogResult
	LOCAL message AS STRING 

	TRY
		response := DialogResult.None
		message := ""
	
		// aboutDialog.Logo = GetIconFromBitmap(GetBitmapFromFile("../EtoFormsApp2/Resources/App.png"), 32, 32);
		message := message + "ProgramName = XSharp Forms App 1\n"
		message := message + "Version = 0.2\n"
		message := message + "Desktop GUI app prototype, on Windows(/Linux/Mac?), in X# / WinForms (/ Mono?), using XIDE\n"
		message := message + "WebsiteLabel = GitLab\n"
		message := message + "Website = http://www.gitlab.com\n"
		// // aboutDialog.Platform;r/o
		message := message + "Copyright = Copyright (C) 2023 Stephen J Sepan\n"
		message := message + "Designers = Stephen J Sepan, Designer2, Designer3\n"
		message := message + "Developers = Stephen J Sepan, Developer2, Developer3\n"
		message := message + "Documenters = Stephen J Sepan, Documenter2, Documenter3\n"
		message := message + "License = License text here\n"
	
		response := MessageBox.Show(SELF, message, "About " + SELF.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
		
		IF response == DialogResult.OK
			//OK is the only option, but the compiler warns (XS0219) if the value is not checked
		ENDIF
	
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD ColorAction() AS VOID/*Task*/
	LOCAL errorMessage AS STRING 
	LOCAL color AS System.Drawing.Color 
	
	TRY    
		errorMessage := ""
		color := System.Drawing.Color.Black
		IF (GetColor(REF color, REF errorMessage))
			SetStatus(color.ToString() + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN
PRIVATE /*ASync*/ METHOD FontAction() AS VOID/*Task*/
	LOCAL errorMessage AS STRING 
	LOCAL font AS System.Drawing.Font 
	LOCAL color AS System.Drawing.Color 
	
	TRY   
		errorMessage := ""
		font := System.Drawing.Font{"Arial", 10}
		color := System.Drawing.Color{}
		IF (GetFont(REF font, REF color, REF errorMessage))
			SetStatus(font.ToString() + "," + color.ToString() + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN

PRIVATE ASYNC METHOD RunAction() AS Task
	TRY
		//AWAIT DoSomething2() 
		SELF:otxtSomeInt:Text := (Int32.Parse(SELF:otxtSomeInt:Text) + 1):ToString()
		SELF:otxtSomeString:Text := DateTime.Now:ToString()
		SELF:ochkSomeBool:Checked := !SELF:ochkSomeBool:Checked
		SELF:odtmSomeDate:Value := SELF:odtmSomeDate:Value:AddDays(1.0)
	CATCH ex AS Exception 
		Console.Error.WriteLine(ex:Message)
	END
RETURN
#endregion Actions

#region Utility
PRIVATE METHOD  StartProgressBar() AS VOID
	SELF:oprgProgress.Value := 33
	SELF:oprgProgress.Style := ProgressBarStyle.Marquee
	SELF:oprgProgress.Visible := TRUE
	//	DoEvents
RETURN

PRIVATE METHOD  StopProgressBar() AS VOID
	//	DoEvents
	SELF:oprgProgress.Visible := FALSE
RETURN

PRIVATE METHOD  StartActionIcon(resourceItem AS Bitmap/*String resourceItemId*/) AS VOID
	IF resourceItem = NULL  
		SELF:opicAction.Image := NULL
	ELSE 
		SELF:opicAction.Image := resourceItem //global::MonoApp1.Properties.Resources.New //"New", etc.
	ENDIF
	SELF:opicAction.Visible := TRUE
RETURN

PRIVATE METHOD StopActionIcon() AS  VOID 
	SELF:opicAction.Visible := FALSE
	SELF:opicAction.Image := NULL //GLOBAL::MonoApp1.Properties.Resources.New
RETURN

PRIVATE METHOD GetPrinter(	printer REF PrinterSettings, errorMessage REF STRING ) AS Boolean
	LOCAL returnValue AS Boolean 
	LOCAL printDialog AS PrintDialog 
	LOCAL response AS DialogResult 
	
	returnValue := FALSE
	TRY
		printDialog := PrintDialog{}
		response := DialogResult.None

		response := printDialog.ShowDialog(SELF)
			
		IF (response == DialogResult.OK)
			printer := printDialog.PrinterSettings
		ELSE //if (response == DialogResult.Cancel)
			SetStatus(ACTION_CANCELLED + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
		
		printDialog.Dispose()
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN returnValue

/// <summary>
/// Get a color.
/// </summary>
/// <param name="color"></param>
/// <param name="errorMessage"></param>
/// <returns></returns>
PRIVATE METHOD GetColor(color  REF System.Drawing.Color ,errorMessage REF STRING ) AS Boolean 
	LOCAL returnValue AS Boolean 
	LOCAL colorDialog AS ColorDialog 
	LOCAL response AS DialogResult 

	returnValue := FALSE
	TRY   
		response := DialogResult.None
		
		colorDialog := ColorDialog{}

		response := colorDialog.ShowDialog(SELF)
		
		IF (response == DialogResult.OK)
			color := colorDialog.Color
			returnValue := TRUE
		ELSE //if (response == DialogResult.Cancel)
			SetStatus(ACTION_CANCELLED + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
	
		colorDialog.Dispose()
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN returnValue

/// <summary>
/// Get a font.
/// </summary>
/// <param name="font"></param>
/// <param name="color"></param>
/// <param name="errorMessage"></param>
/// <returns></returns>
PRIVATE METHOD GetFont(	font REF System.Drawing.Font ,color REF System.Drawing.Color, errorMessage REF STRING ) AS Boolean 
	LOCAL returnValue AS Boolean 
	LOCAL fontDialog AS FontDialog 
	LOCAL response AS DialogResult 

	returnValue := FALSE
	TRY   
		response := DialogResult.None
		// fontResponse := null;
		fontDialog := FontDialog{}

		response := fontDialog.ShowDialog(SELF)
		
		IF (response == DialogResult.OK)
			font := fontDialog.Font
			color := fontDialog.Color   
			returnValue := TRUE
		ELSE //if (response == DialogResult.Cancel)
			SetStatus(ACTION_CANCELLED + ACTION_IN_PROGRESS, TRUE) 
		ENDIF
	
		fontDialog.Dispose()
	CATCH ex AS Exception
		SetError(ex:Message, FALSE)
		Console.Error.WriteLine(ex:Message)
	END
RETURN returnValue 

  
// The purpose of these is to set a Tooltip at the same time as Text;
//  a Tooltip was added at the form level.

/// <summary>
/// Set status message.
/// </summary>
/// <param name="message">STRING</param>
/// <param name="append">Boolean</param>
/// <returns></returns>
PRIVATE METHOD SetStatus(message AS STRING, append AS Boolean) AS VOID
	TRY
		IF append
			SELF:olblStatus:Text := SELF:olblStatus.Text + message
		ELSE
			SELF:olblStatus:Text :=  message
		ENDIF
		SELF:oTooltip1.SetToolTip(SELF:olblStatus, SELF:olblStatus.Text)
	CATCH ex AS Exception
		Console.Error.WriteLine(ex:Message)
	END
RETURN

/// <summary>
/// Set error message.
/// </summary>
/// <param name="message">STRING</param>
/// <param name="append">Boolean</param>
/// <returns></returns>
PRIVATE METHOD SetError(message AS STRING, append AS Boolean) AS VOID
	TRY
		IF append
			SELF:olblError.Text := SELF:olblError.Text + message
		ELSE
			SELF:olblError.Text := message
		ENDIF
		SELF:oTooltip1.SetToolTip(SELF:olblError, SELF:olblError.Text)
	CATCH ex AS Exception
		Console.Error.WriteLine(ex:Message)
	END
RETURN

#endregion Utility
#endregion Methods

END CLASS

