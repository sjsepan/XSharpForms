# XSharpFormsApp1

## About

Desktop GUI app prototype, on Windows/Linux(/Mac?), in X# with WinForms/Mono, using XIDE

![XSharpFormsApp1.png](./XSharpFormsApp1.png?raw=true "Screenshot")

## Issues

-"error XS0117: 'System.Windows.Forms.Keys' does not contain a definition for 'Del'	388,39	FrmMain.prg	BasicForm:InitializeForm" 
	`SELF:omenuEditDelete:ShortcutKeys := System.Windows.Forms.Keys.Del + System.Windows.Forms.Keys.Control`
You can manually change the property in code, from .Del to .Delete, and it will compile, but the next time you save the form from the designer the code will revert.
-FrmMain.prg contains a 'METHOD placeholder() AS VOID' after the designer-generated code. Note: if this method is not present, changes to the form designer will erase the #region tags below when the changes are saved. The designer respects the presence of keywords, but not pre-processor directives.
-"libpng warning: bKGD: invalid" when running with mono on Linux. TBD

## Caveats

-The 'Name' property of a control in the designer is not the same as the variable created in the code-behind. For example, a control named 'lblStatus' will be backed by a variable named 'olblStatus'. Note: it *does* match the value in the .wed file, but that is not what your code-behind is looking for. A working rule-of-thumb seems to be to use 'o' (in the code) to prefix the value from the Name property from the designer. Note: in XIDE, under Tools|Preferences, in the Designers tab, 'Code generation' section, change 'Control Prefix' from 'o' to 'None'.
-When passing By Reference, the REF keyword replaces the AS keyword in the formal parameter list. The language reference does indicate this ("AS|REF|OUT|IN"), but it can be easy to overlook.
-Const has the 'AS type' clause in the initializer after the literal.

### History

0.2:
~figured out CONST syntax and used for message literals

0.1:
~initial release

Steve Sepan
ssepanus@yahoo.com
7/3/2023